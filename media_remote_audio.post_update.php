<?php

/**
 * @file
 * Contains post-update hooks for Media Remote Audio.
 */

/**
 * Enables SoundCloud-specific workarounds.
 */
function media_remote_audio_post_update_enable_soundcloud_workarounds() {
  // Deliberately empty in order to force a container rebuild.
}

/**
 * Ensures the remote audio media source has the correct metadata attributes.
 */
function media_remote_audio_post_update_fix_thumbnail_metadata_attributes() {
  // Deliberately empty in order to force a container rebuild.
}
