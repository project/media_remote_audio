<?php

namespace Drupal\Tests\media_remote_audio\Kernel;

use Drupal\KernelTests\KernelTestBase;
use Drupal\media\Entity\Media;
use Drupal\media_remote_audio_test\ResourceMiddleware;

/**
 * Tests creating media items from remote audio sources.
 *
 * @group media_remote_audio
 */
class RemoteAudioMediaTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'field',
    'file',
    'image',
    'media',
    'media_remote_audio',
    'media_remote_audio_test',
    'system',
    'user',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    // Set up the database so that we can create remote audio media items.
    $this->installConfig('media');
    $this->installConfig('media_remote_audio');
    $this->installEntitySchema('file');
    $this->installEntitySchema('media');
    $this->installEntitySchema('user');
    $this->installSchema('file', ['file_usage']);
  }

  /**
   * Data provider for ::testRemoteAudioMedia().
   *
   * @return array[]
   *   The test cases.
   */
  public static function providerRemoteAudioMedia(): array {
    return [
      'iHeartRadio' => [
        'https://www.iheart.com/podcast/270-crime-junkie-29319113/episode/murdered-jenny-lin-part-2-101263716/',
        __DIR__ . '/../../fixtures/iHeartRadio.json',
      ],
      'Spotify' => [
        'https://open.spotify.com/album/42YnG5klTs8VUflewCZamw',
        __DIR__ . '/../../fixtures/Spotify.json',
      ],
    ];
  }

  /**
   * Tests creating remote audio media from various providers.
   *
   * @param string $url
   *   The URL of the remote media to add. In a real site, this would be used to
   *   create the media in the UI.
   * @param string $fixture_file
   *   The path of the local file containing the oEmbed resource data.
   *
   * @dataProvider providerRemoteAudioMedia
   */
  public function testRemoteAudioMedia(string $url, string $fixture_file): void {
    $media = $this->createRemoteAudioMedia($url, $fixture_file);

    // The resource title should be reused as the alt text of the thumbnail.
    $source = $media->getSource();
    $plugin_definition = $source->getPluginDefinition();
    $this->assertSame($media->label(), $source->getMetadata($media, $plugin_definition['thumbnail_alt_metadata_attribute']));
  }

  /**
   * Tests thumbnail size resolution when creating SoundCloud media.
   *
   * @param string $fixture_file
   *   The path of the local file containing the oEmbed resource data.
   * @param int $expected_thumbnail_width
   *   The expected width of the thumbnail.
   * @param int $expected_thumbnail_height
   *   The expected height of the thumbnail.
   *
   * @testWith ["SoundCloud-thumbnail_size_in_url.xml", 500, 500]
   *   ["SoundCloud-no_thumbnail_size.xml", 200, 200]
   */
  public function testSoundCloudThumbnailSize(string $fixture_file, int $expected_thumbnail_width, int $expected_thumbnail_height): void {
    $fixture_file = __DIR__ . '/../../fixtures/' . $fixture_file;

    $media = $this->createRemoteAudioMedia('https://soundcloud.com/dj-aphrodite/drum-and-bass-style-dj-studio-mix-2016-2017', $fixture_file);
    $source = $media->getSource();
    $this->assertSame($expected_thumbnail_width, $source->getMetadata($media, 'thumbnail_width'));
    $this->assertSame($expected_thumbnail_height, $source->getMetadata($media, 'thumbnail_height'));
  }

  /**
   * Creates a remote audio media item.
   *
   * @param string $url
   *   The URL of the remote media to add. In a real site, this would be used to
   *   create the media in the UI.
   * @param string $fixture_file
   *   The path of the local file containing the oEmbed resource data.
   *
   * @return \Drupal\media\Entity\Media
   *   The created media item.
   */
  private function createRemoteAudioMedia(string $url, string $fixture_file): Media {
    // Ensure the resource data can be fetched by the test middleware.
    $resource_url = $this->container->get('media.oembed.url_resolver')
      ->getResourceUrl($url);

    /** @var \Drupal\Core\State\StateInterface $state */
    $state = $this->container->get('state');
    $paths = [
      $resource_url => $fixture_file,
    ];
    $state->set(ResourceMiddleware::class, $paths);

    // Ensure the the thumbnail can be fetched by our test middleware.
    $thumbnail_url = $this->container->get('media.oembed.resource_fetcher')
      ->fetchResource($resource_url)
      ->getThumbnailUrl();
    $this->assertNotEmpty($thumbnail_url);
    $thumbnail_url = $thumbnail_url->toString();
    $paths[$thumbnail_url] = $this->getDrupalRoot() . '/core/misc/druplicon.png';
    $state->set(ResourceMiddleware::class, $paths);

    // Ensure we can create a remote audio media item from the given fixture.
    $media = Media::create([
      'bundle' => 'remote_audio',
      'field_media_oembed_audio' => $url,
    ]);
    $media->save();

    return $media;
  }

}
